# [1.1.0](https://gitlab.com/dreamer-labs/piperci/piperci-cookiecutter-command/compare/v1.0.0...v1.1.0) (2019-09-30)


### Features

* modify config values ([a1a4e49](https://gitlab.com/dreamer-labs/piperci/piperci-cookiecutter-command/commit/a1a4e49))

# 1.0.0 (2019-09-13)


### Bug Fixes

* **tests:** fix/remove broken/uneeded unittests in template ([7b76706](https://gitlab.com/dreamer-labs/piperci/piperci-cookiecutter-command/commit/7b76706))


### Features

* ****/handler.validate:** Add support for 1+ commands in one request ([f1caaf1](https://gitlab.com/dreamer-labs/piperci/piperci-cookiecutter-command/commit/f1caaf1))
* convert to cookiecutter template for command faas's ([344843d](https://gitlab.com/dreamer-labs/piperci/piperci-cookiecutter-command/commit/344843d)), closes [#1](https://gitlab.com/dreamer-labs/piperci/piperci-cookiecutter-command/issues/1)
* echo_faas executes passed run-cmd ([0e5cf9a](https://gitlab.com/dreamer-labs/piperci/piperci-cookiecutter-command/commit/0e5cf9a))
