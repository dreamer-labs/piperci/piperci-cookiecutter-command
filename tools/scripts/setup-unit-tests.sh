#!/bin/bash

FUNCTION_TEMPDIR=$1
FUNCTION_TEMPLATE=$2

THIS_SCRIPT=$(basename "${BASH_SOURCE[0]}")
THIS_RELDIR=$(dirname "${BASH_SOURCE[0]}")
REPO_BASEDIR=$(cd $THIS_RELDIR; cd ../../; pwd)

COOKIECUTTER_DIR=${FUNCTION_TEMPDIR}/piperci-cookiecutter-command/

[[ -d ${FUNCTION_TEMPDIR} ]] && rm -r ${FUNCTION_TEMPDIR};

mkdir -p ${COOKIECUTTER_DIR}

export REPO_BASEDIR
IFS=$'\n'

cp -v ${REPO_BASEDIR}/setup.py ${COOKIECUTTER_DIR}/
cp -v ${REPO_BASEDIR}/cookiecutter.json ${COOKIECUTTER_DIR}/
cp -rv ${REPO_BASEDIR}/\{\{cookiecutter.project_name\}\} ${COOKIECUTTER_DIR}/

pushd ${FUNCTION_TEMPDIR}
cookiecutter --no-input piperci-cookiecutter-command

rm -rf piperci-cookiecutter-command
touch __init__.py

popd

mv ${FUNCTION_TEMPDIR}/piperci-echo-faas/tests ${FUNCTION_TEMPDIR}

function_names=(piperci_echo_executor piperci_echo_gateway)
for func_name in ${function_names[@]}; do
  mkdir -p ${FUNCTION_TEMPDIR}/$func_name/function
  cp -r ${FUNCTION_TEMPLATE}/piperci-flask/* ${FUNCTION_TEMPDIR}/$func_name/
  cp -v ${FUNCTION_TEMPDIR}/piperci-echo-faas/$func_name/*.py ${FUNCTION_TEMPDIR}/$func_name/function/
  cp ${FUNCTION_TEMPDIR}/$func_name/faas_app.py ${FUNCTION_TEMPDIR}/$func_name/function/faas_app.py
done
